import math
try:
    k=int(input("Введите целое число\n"))
    while k<=0:
        k=int(input("Введите целое число\n"))
except:
    raise ValueError('Error')
if k==1:
    print("2")
else:    
    def bit_sieve( n ):
        if n < 2:
            return []
        bits = [ 1 ] * n                            
        sqrt_n  = int( math.sqrt( n ) ) + 1
        for i in range( 2, sqrt_n ):
            if bits[ i - 2 ]:                        # если i -- простое
                for j in range ( i + i, n + 1, i ):  # занулить все ему кратные
                    bits[ j - 2 ] = 0
        return bits 
    
    # k-ое простое не превосходит 1,5 k ln( k ) при k > 1: 
    sieve = bit_sieve( int(1.5*k*math.log( k ) ) + 1 )
    i = 0
    while k:

        k -= sieve[ i ]
        i += 1
    print( i + 1 )
